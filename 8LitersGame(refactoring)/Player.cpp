#include "Player.h"



Player::Player()
{
}


Player::~Player()
{
}

Player::Player(string pName, float pScore, int pChance)
{
	this->playerName = pName;
	this->playerScore = pScore;
	this->playerChance = pChance;
}

string Player::getPlayerName()
{
	return playerName;
}

float Player::getPlayerScore()
{
	return playerScore;
}

int Player::getPlayerChance()
{
	return playerChance;
}

void Player::setNewChance()
{
	playerChance--;
}

void Player::setNewScore(Status status)
{
	switch (status)
	{
	case doingRightStep:
		playerScore += playerChance * 0.5;
		break;
	case doingWrongStep:
		playerScore -= playerChance * 0.1;
		break;
	case windOnTheGame:
		playerScore += 100;
		break;
	}
}

void Player::savePlayerInfo()
{
	playerFile.open("players-saved.txt", ios::out | ios::app);

	playerFile << playerName << "\n" << playerScore << " " << playerChance << endl;

	playerFile.close();
}

void Player::showPlayerInfo()
{
	cout << "/* " << playerName << " */" << endl;
	cout << "Score: " << playerScore << endl;
	cout << "Chance: " << playerChance << endl << endl;
}
