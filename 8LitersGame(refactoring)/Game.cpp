#include "Game.h"

Game::Game()
{
	this->selected = 1;
	this->isLoad = false;
}

Game::~Game()
{
}

void Game::start()
{
	initGame();
}

/* GLASS */
void Game::initGlass()
{	
	glasses.clear();
	
	Glass *initGlass;
	
	initGlass = new NormalGlass(
		NORMAL_GLASS_POSITION, 
		NORMAL_GLASS_CAPACITY,
		NORMAL_GLASS_VOLUME_OF_WATER,
		NORMAL_GLASS_VOLUME_OF_OIL
	);
	glasses.push_back(initGlass);

	initGlass = new MagicGlass(
		MAGIC_GLASS_POSITION,
		MAGIC_GLASS_CAPACITY,
		MAGIC_GLASS_VOLUME_OF_WATER,
		MAGIC_GLASS_VOLUME_OF_OIL
	);
	glasses.push_back(initGlass);

	initGlass = new HotGlass(
		HOT_GLASS_POSITION,
		HOT_GLASS_CAPACITY,
		HOT_GLASS_VOLUME_OF_WATER,
		HOT_GLASS_VOLUME_OF_OIL,
		HOT_GLASS_INITIAL_TEMPERATURE
	);
	glasses.push_back(initGlass);
}

bool Game::isPouringPlacesEmpty()
{
	return !gTop && !gBottom ? true : false;
}

void Game::glassSelection()
{
	do {
		getOutGlasses();
		showStatus();
		gameStatus();
		temperatureEffect();

		cout << "Press 0 to break or another to continue . . .\n>> ";
		cin >> choice;
		if (choice == 0) break;

		int temp = btIdx;

		cout << "Select a glass to put on top: "; cin >> topIdx;
		cout << "Select a glass to put on bottom: "; cin >> btIdx;
		
		playerScore(topIdx, btIdx);
		 
		switch (btIdx) {
		case 1:
			btIdx = temp;
			break;
		case 2:
			btIdx = temp + 1;
			break;
		case 3:
			btIdx = temp + 2;
			break;
		}

		switch (topIdx) {
		case 1:
			topIdx = temp;
			break;
		case 2:
			topIdx = temp + 1;
			break;
		case 3:
			topIdx = temp + 2;
			break;
		}

		if (isPouringPlacesEmpty()) {
			glasses[topIdx]->setNewPosition(putOnTop);
			gTop = glasses[topIdx];
			glasses[btIdx]->setNewPosition(putOnBottom);
			gBottom = glasses[btIdx];
		}
		
		pouringProcess();
	} while (true);
}

void Game::temperatureEffect()
{
	int temp = btIdx;
	if (glasses[temp + 2]->getCharge() != 0) {
		if (glasses[temp + 2]->getGlassTemperature() >= WATER_BOILING_POINT) {
			glasses[temp + 2]->setNewVolumeOfFluid(water);
		}
		if (glasses[temp + 2]->getGlassTemperature() >= OIL_BOILING_POINT) {
			glasses[temp + 2]->setNewVolumeOfFluid(oil);
		}
	}
	else {
		glasses[temp + 2]->getGlassTemperature();
	}
	
}

void Game::getOutGlasses()
{
	for (int i = 0; i < glasses.size(); i++) {
		glasses[i]->setNewPosition(getOut);
	}

	gTop = NULL;
	gBottom = NULL;
}


/* PLAYER */
void Game::newPlayer()
{
	this->isLoad = false;
	
	string newPlayerName;
	Player *newPlayer;
	
	cout << "Your name: ";
	cin.ignore();
	getline(cin, newPlayerName);
	
	newPlayer = new Player(
		newPlayerName,
		INITIAL_PLAYER_SCORE,
		INITIAL_PLAYER_CHANCE
	);
	players.push_back(newPlayer);
	
	selectedPlayer = newPlayer;
	
	showCommand();
}

void Game::playerScore(int topIdx, int btIdx)
{
	switch (selectedPlayer->getPlayerChance()) {
	case 1: case 5: case 7: case 11: case 13:
		if (topIdx == 1 && btIdx == 3)
			selectedPlayer->setNewScore(doingRightStep);
		else
			selectedPlayer->setNewScore(doingWrongStep);
		break;
	case 3: case 9:
		if (topIdx == 2 && btIdx == 1)
			selectedPlayer->setNewScore(doingRightStep);
		else
			selectedPlayer->setNewScore(doingWrongStep);
		break;
	case 2: case 4: case 6: case 8: case 10: case 12:
		if (topIdx == 3 && btIdx == 2)
			selectedPlayer->setNewScore(doingRightStep);
		else
			selectedPlayer->setNewScore(doingWrongStep);
		break;
	}
}


/* GAME */
void Game::mainMenu()
{
	system("cls");
	cout << "MAIN MENU" << endl;
	cout << "1. New Game" << endl;
	cout << "2. Load Game" << endl;
	cout << "3. Show Leaderboard" << endl;
	cout << "4. Reset History" << endl;
	cout << "5. Quit" << endl;
	cout << ">> ";
	cin >> choice;
}

void Game::saveGame()
{
	selectedPlayer->savePlayerInfo();

	for (int i = 0; i < 3; i++) {
		glasses[i]->saveGlassInfo();
	}

	cout << "Game succesfully to save . . ." << endl;
	system("pause");
}

void Game::loadPlayerInfo()
{
	pList.clear();

	Player *newPlayer;

	playerFile.open("players-saved.txt", ios::in);

	string playerName;
	float playerScore;
	int playerChance;
	
	while (true) {
		getline(playerFile, playerName);
		playerFile >> playerScore >> playerChance;

		if (playerFile.eof()) break;

		newPlayer = new Player(playerName, playerScore, playerChance);
		pList.push_back(newPlayer);

		playerFile.ignore();
	}
	playerFile.close();

	for (int i = 0; i < pList.size(); i++) {
		cout << "[" << i + 1 << "]" << endl;
		pList[i]->showPlayerInfo();
	}

	if (pList.size() != 0) {
		cout << "Choose player to load:\n>> ";
		cin >> selected;
		selectedPlayer = pList[selected - 1];
	}
	else {
		cout << "No players to loaded!" << endl;
		system("pause");
		
		initGame();
	}
}

void Game::loadGlassInfo()
{
	gList.clear();
	glasses.clear();

	Glass *newGlass;

	glassFile.open("glasses-saved.txt", ios::in);

	string gType;
	char gPosition;
	int gCapacity;
	float vOfWater;
	float vOfOil;
	float gTemperature;

	while (true) {
		getline(glassFile, gType);
		
		if (glassFile.eof()) break;

		if (gType == NORMAL_GLASS_TYPE) {
			glassFile >> gPosition >> gCapacity >> vOfWater >> vOfOil;
			newGlass = new NormalGlass(gPosition, gCapacity, vOfWater, vOfOil);
			gList.push_back(newGlass);
			glassFile.ignore();
		}
		else if (gType == MAGIC_GLASS_TYPE) {
			glassFile >> gPosition >> gCapacity >> vOfWater >> vOfOil;
			newGlass = new MagicGlass(gPosition, gCapacity, vOfWater, vOfOil);
			gList.push_back(newGlass);
			glassFile.ignore();
		}
		else if (gType == HOT_GLASS_TYPE) {
			glassFile >> gPosition >> gCapacity >> vOfWater >> vOfOil >> gTemperature;
			newGlass = new HotGlass(gPosition, gCapacity, vOfWater, vOfOil, gTemperature);
			gList.push_back(newGlass);
			glassFile.ignore();
		}
	}

	glassFile.close();
}

void Game::loadGame()
{	
	this->isLoad = true;
	
	loadPlayerInfo();
	loadGlassInfo();

	showCommand();
}

bool sortByScore(Player *player1, Player *player2)
{
	return player1->getPlayerScore() > player2->getPlayerScore();
}

void Game::showLeaderBoard()
{	
	pRank.clear();

	Player *loadPlayer;

	playerFile.open("players-saved.txt", ios::in);

	string gName;
	float gScore;
	int gChance;

	while (true) {
		getline(playerFile, gName);
		playerFile >> gScore;
		playerFile >> gChance;

		if (playerFile.eof()) break;

		loadPlayer = new Player(gName, gScore, gChance);
		pRank.push_back(loadPlayer);

		playerFile.ignore();
	}
	playerFile.close();

	sort(pRank.begin(), pRank.end(), sortByScore);

	system("cls");
	cout << "LEADERBOARD" << endl;
	for (int i = 0; i < pRank.size(); i++) {
		cout << i + 1 << ". " << pRank[i]->getPlayerName()
			<< " [" << pRank[i]->getPlayerScore() << "]" << endl;
	}
	system("pause");
}

void Game::resetHistory()
{
	playerFile.open("players-saved.txt", ios::out | ios::trunc);
	glassFile.open("glasses-saved.txt", ios::out | ios::trunc);

	playerFile.close();
	glassFile.close();

	cout << "Game history has been to reset." << endl;
	system("pause");
}

void Game::selectAction()
{
	system("cls");
	cout << "SELECT ACTION: " << endl;
	cout << "1. Select glass" << endl;
	cout << "2. Save game" << endl;
	cout << "3. Back to main menu" << endl;
	cout << ">> ";
	cin >> choice;
}

void Game::pouringProcess()
{	
	selectedPlayer->setNewChance();

	t.vOfOil = gTop->getVolumeOfOil();
	t.vOfWater = gTop->getVolumeOfWater();
	b.vOfOil = gBottom->getVolumeOfOil();
	b.vOfWater = gBottom->getVolumeOfWater();

	if (gBottom->getFreeSpace() < gTop->getCharge()) {
		if (gBottom->getFreeSpace() > gTop->getVolumeOfOil()) {
			gBottom->setNewVolumeOfOil(b.vOfOil + t.vOfOil);
			gTop->setNewVolumeOfOil(0);
			if (gBottom->getFreeSpace() != 0) {
				if (gBottom->getFreeSpace() > gTop->getVolumeOfWater()) {
					gBottom->setNewVolumeOfWater(b.vOfWater + t.vOfWater);
					gTop->setNewVolumeOfWater(0);
				}
				else {
					float temp = gBottom->getFreeSpace();
					gBottom->setNewVolumeOfWater(b.vOfWater + temp);
					gTop->setNewVolumeOfWater(t.vOfWater - temp);
				}
			}
		}
		else {
			float temp = gBottom->getFreeSpace();
			gBottom->setNewVolumeOfOil(b.vOfOil + temp);
			gTop->setNewVolumeOfOil(t.vOfOil -= temp);
		}
	}
	else {
		gTop->setNewVolumeOfOil(0);
		gTop->setNewVolumeOfWater(0);
		gBottom->setNewVolumeOfOil(b.vOfOil + t.vOfOil);
		gBottom->setNewVolumeOfWater(b.vOfWater + t.vOfWater);
	}
}

void Game::showStatus()
{
	system("cls");

	selectedPlayer->showPlayerInfo();

	if (isLoad) {
		glasses = gList;
		isLoad = false;
	}
	else {
		selected = 1;
	}

	btIdx = (selected - 1) * 3;
	topIdx = btIdx + 3;

	cout << "# CURRENT STATUS #" << endl;
	for (int i = btIdx; i < topIdx; i++) {
		if (!isLoad) glasses[i]->showGlassInfo();
		else gList[i]->showGlassInfo();
		cout << endl;
	}
}

void Game::gameStatus()
{
	selectedPlayer->getPlayerChance() == 0 ? gameOver = true : gameOver = false;

	if (gameOver) {
		if (glasses[selected - 1]->getCharge() == DEFAULT_CHARGE_TO_WIN) {
			cout << "Yay, you successfully to solve the game!" << endl;
			cout << "Would you save your achievment?\n>> ";
			cin >> choice;
			if (choice == 1) saveGame();
		}
		else {
			cout << "You failed to solve the game!" << endl;
			system("pause");
		}
		initGame();
	}
}

void Game::showCommand()
{
	while (true) {
		selectAction();

		switch (choice) {
		case 1:
			glassSelection();
			break;
		case 2:
			saveGame();
			break;
		case 3:
			initGame();
			break;
		}
	}
}

void Game::initGame()
{
	initGlass();
	
	while (true) {
		mainMenu();

		if (choice == 5) break;

		switch (choice) {
		case 1:
			newPlayer();
			break;
		case 2:
			loadGame();
			break;
		case 3:
			showLeaderBoard();
			break;
		case 4:
			resetHistory();
			break;
		}
	}
}
