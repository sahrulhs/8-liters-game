#include "HotGlass.h"


HotGlass::HotGlass()
{
}


HotGlass::~HotGlass()
{
}

HotGlass::HotGlass(char gPosition, int gCapacity, float vOfWater, float vOfOil, float gTemperature)
{
	this->glassPosition = gPosition;
	this->glassCapacity = gCapacity;
	this->volumeOfWater = vOfWater;
	this->volumeOfOil = vOfOil;
	this->glassTemperature = gTemperature;
}

float HotGlass::getGlassTemperature()
{
	if (getCharge() == 0) {
		glassTemperature = 25.0;
	}
	else {
		glassTemperature += glassTemperature * 0.20;
	}

	return glassTemperature;
}

void HotGlass::showGlassInfo()
{
	cout << "/Hot Glass/" << endl;
	cout << "Capacity    => " << glassCapacity << " L" << endl;
	cout << "Position    => " << glassPosition << endl;
	cout << "Charge      => " << volumeOfWater << " L (water)" << " + " 
		<< volumeOfOil << " L (oil)" << " = " << getCharge() << " L" << endl;

	cout << "Temperature => " << glassTemperature << " derajat C" << endl;
}

void HotGlass::saveGlassInfo()
{
	glassFile.open("glasses-saved.txt", ios::out | ios::app);

	glassFile << "Hot Glass" << "\n" << glassPosition << " " 
		<< glassCapacity << " " << volumeOfWater << " " 
		<< volumeOfOil << " " << glassTemperature << endl;

	glassFile.close();
}
