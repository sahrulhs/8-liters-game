#include "NormalGlass.h"


NormalGlass::NormalGlass()
{
}


NormalGlass::~NormalGlass()
{
}

NormalGlass::NormalGlass(char gPosition, int gCapacity, float vOfWater, float vOfOil)
{
	this->glassPosition = gPosition;
	this->glassCapacity = gCapacity;
	this->volumeOfWater = vOfWater;
	this->volumeOfOil = vOfOil;
}

void NormalGlass::showGlassInfo()
{
	cout << "/Normal Glass/" << endl;
	cout << "Capacity  => " << getGlassCapacity() << " L" << endl;
	cout << "Position  => " << getGlassPosition() << endl;
	cout << "Charge    => " << getVolumeOfWater() << " L (water)" 
		<< " + " << getVolumeOfOil() << " L (oil)" << " = " 
		<< getCharge() << " L" << endl;
}

void NormalGlass::saveGlassInfo()
{
	glassFile.open("glasses-saved.txt", ios::out | ios::app);

	glassFile << "Normal Glass" << "\n" << glassPosition 
		<< " " << glassCapacity << " " << volumeOfWater 
		<< " " << volumeOfOil << endl;

	glassFile.close();
}
