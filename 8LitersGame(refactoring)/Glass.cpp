#include "Glass.h"



Glass::Glass()
{
}


Glass::~Glass()
{
}

void Glass::setNewPosition(Action action)
{
	switch (action) {
	case putOnTop:
		glassPosition = 'T';
		break;
	case putOnBottom:
		glassPosition = 'B';
		break;
	case getOut:
		glassPosition = 'G';
		break;
	}
}

void Glass::setNewVolumeOfWater(int vOfWater)
{
	this->volumeOfWater = vOfWater;
}

void Glass::setNewVolumeOfOil(int vOfOil)
{
	this->volumeOfOil = vOfOil;
}

void Glass::setNewVolumeOfFluid(Fluid fluid)
{
	switch (fluid)
	{
	case water:
		if (volumeOfWater >= 1.0) volumeOfWater -= 1.0;
		else volumeOfWater = 0;
		break;
	case oil:
		if (volumeOfOil >= 0.5) volumeOfOil -= 0.5;
		else volumeOfOil = 0;
		break;
	default:
		break;
	}
}

char Glass::getGlassPosition()
{
	return glassPosition;
}

int Glass::getGlassCapacity()
{
	return glassCapacity;
}

float Glass::getGlassTemperature()
{
	return 0.0;
}

float Glass::getVolumeOfWater()
{
	if (volumeOfWater <= 0) volumeOfWater = 0;
	return volumeOfWater;
}

float Glass::getVolumeOfOil()
{
	if (volumeOfOil <= 0) volumeOfOil = 0;
	return volumeOfOil;
}

float Glass::getCharge()
{
	return volumeOfWater + volumeOfOil;
}

float Glass::getFreeSpace()
{
	return getGlassCapacity() - getCharge();
}
