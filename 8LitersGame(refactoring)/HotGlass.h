#pragma once
#include "Glass.h"

class HotGlass :
	public Glass
{
public:
	HotGlass();
	~HotGlass();

	HotGlass(char, int, float, float, float);

	float getGlassTemperature();

private:
	float glassTemperature;

	void showGlassInfo();
	void saveGlassInfo();
};

