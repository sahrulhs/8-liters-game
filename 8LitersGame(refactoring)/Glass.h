#pragma once
#include "header.h"

enum Action {
	putOnTop,
	putOnBottom,
	getOut
};

enum Fluid {
	water,
	oil
};

class Glass
{
public:
	Glass();
	~Glass();

	virtual void showGlassInfo() = 0;
	virtual void saveGlassInfo() = 0;
	
	virtual void setNewPosition(Action);
	
	virtual void setNewVolumeOfWater(int);
	virtual void setNewVolumeOfOil(int);

	virtual void setNewVolumeOfFluid(Fluid);
	
	virtual char getGlassPosition();
	virtual int getGlassCapacity();
	
	virtual float getGlassTemperature();
	virtual float getVolumeOfWater();
	virtual float getVolumeOfOil();
	virtual float getCharge();
	virtual float getFreeSpace();

protected:
	char glassPosition;
	int glassCapacity;
	float volumeOfWater;
	float volumeOfOil;

	fstream glassFile;
};

