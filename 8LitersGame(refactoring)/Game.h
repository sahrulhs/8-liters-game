#pragma once
#include "header.h"
#include "NormalGlass.h"
#include "MagicGlass.h"
#include "HotGlass.h"
#include "Player.h"

#define NORMAL_GLASS_TYPE "Normal Glass"
#define NORMAL_GLASS_POSITION 'G'
#define NORMAL_GLASS_CAPACITY 16
#define NORMAL_GLASS_VOLUME_OF_WATER 8.0
#define NORMAL_GLASS_VOLUME_OF_OIL 8.0

#define MAGIC_GLASS_TYPE "Magic Glass"
#define MAGIC_GLASS_POSITION 'G'
#define MAGIC_GLASS_CAPACITY 11
#define MAGIC_GLASS_VOLUME_OF_WATER 0.0
#define MAGIC_GLASS_VOLUME_OF_OIL 0.0
#define MAGIC_GLASS_VOLUME_OF_DETERGENT 0

#define HOT_GLASS_TYPE "Hot Glass"
#define HOT_GLASS_POSITION 'G'
#define HOT_GLASS_CAPACITY 6
#define HOT_GLASS_VOLUME_OF_WATER 0.0
#define HOT_GLASS_VOLUME_OF_OIL 0.0
#define HOT_GLASS_INITIAL_TEMPERATURE 20.0

#define WATER_BOILING_POINT 100.0
#define OIL_BOILING_POINT 180.0

#define INITIAL_PLAYER_SCORE 0.0
#define INITIAL_PLAYER_CHANCE 13

#define DEFAULT_CHARGE_TO_WIN 8

class Game
{
public:
	Game();
	~Game();

	void start();

private:
	int choice, selected, topIdx, btIdx;
	bool isLoad, gameOver;

	/* GLASS */
	fstream glassFile;
	vector<Glass*> glasses, gList;
	Glass *gTop, *gBottom;
	
	struct GlassProperties {
		float vOfWater;
		float vOfOil;
	} t, b;

	void initGlass();
	bool isPouringPlacesEmpty();
	void glassSelection();
	void temperatureEffect();
	void getOutGlasses();

	/* PLAYER */
	fstream playerFile;
	vector<Player*> players, pList, pRank;
	Player *selectedPlayer;

	void newPlayer();
	void playerScore(int, int);

	/* GAME */
	void mainMenu();
	
	void saveGame();
	void loadGlassInfo();
	void loadPlayerInfo();
	void loadGame();
	void showLeaderBoard();

	void resetHistory();
	void selectAction();
	void pouringProcess();
	void showStatus();
	
	void gameStatus();

	void showCommand();
	void initGame();
};