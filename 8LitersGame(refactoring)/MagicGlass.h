#pragma once
#include "Glass.h"
class MagicGlass :
	public Glass
{
public:
	MagicGlass();
	~MagicGlass();

	MagicGlass(char, int, float, float);

private:
	float getCharge();

	void showGlassInfo();
	void saveGlassInfo();
};

