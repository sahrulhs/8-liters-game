#include "MagicGlass.h"



MagicGlass::MagicGlass()
{
}


MagicGlass::~MagicGlass()
{
}

MagicGlass::MagicGlass(char gPosition, int gCapacity, float vOfWater, float vOfOil)
{
	this->glassPosition = gPosition;
	this->glassCapacity = gCapacity;
	this->volumeOfWater = vOfWater;
	this->volumeOfOil = vOfOil;
}


float MagicGlass::getCharge()
{
	return volumeOfWater + volumeOfOil;
}

void MagicGlass::showGlassInfo()
{
	cout << "/Magic Glass/" << endl;
	cout << "Capacity  => " << glassCapacity << " L" << endl;
	cout << "Position  => " << glassPosition << endl;
	cout << "Charge    => " << volumeOfWater  << " L (water)"<< " + " 
		<< volumeOfOil << " L (oil) + " << getCharge() << " L" << endl;
}

void MagicGlass::saveGlassInfo()
{
	glassFile.open("glasses-saved.txt", ios::out | ios::app);

	glassFile << "Magic Glass" << "\n" << glassPosition << " " 
		<< glassCapacity << " " << volumeOfWater << " " 
		<< volumeOfOil << endl;

	glassFile.close();
}
