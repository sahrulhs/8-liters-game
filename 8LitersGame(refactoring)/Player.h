#pragma once
#include  "header.h"

enum Status {
	doingRightStep,
	doingWrongStep,
	windOnTheGame
};

class Player
{
public:
	Player();
	~Player();

	Player(string, float, int);

	string getPlayerName();
	float getPlayerScore();
	int getPlayerChance();
	
	void setNewChance();
	void setNewScore(Status);
	void savePlayerInfo();

	void showPlayerInfo();

private:
	string playerName;
	float playerScore;
	int playerChance;

	fstream playerFile;
};

